/* ------------------------------------------------------------------------------
 *
 *  # Echarts - pies and donuts
 *
 *  Pies and donuts chart configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            var rose_diagram_hidden = ec.init(document.getElementById('rose_diagram_hidden'), limitless);

            // Data style
            var dataStyle = {
                normal: {
                    label: {show: false},
                    labelLine: {show: false}
                }
            };

            // Placeholder style
            var placeHolderStyle = {
                normal: {
                    color: 'rgba(0,0,0,0)',
                    label: {show: false},
                    labelLine: {show: false}
                },
                emphasis: {
                    color: 'rgba(0,0,0,0)'
                }
            };

            //
            // Nightingale roses with hidden labels options
            //

            rose_diagram_hidden_options = {

                // Add title
                title: {
                    text: 'Жалобы за первое полугодие. 2007 год',
                    subtext: 'Учтены все выбранные компании (по умолчанию - все компании)',
                    x: 'center'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} - {c} жалоб"
                },

                // Add legend
                legend: {
                    x: 'left',
                    y: 'top',
                    orient: 'vertical',
                    data: ['Январь','Февраль','Март','Апрель','Май','Июнь']
                },

                // Display toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    feature: {
                        magicType: {
                            show: true,
                            title: {
                                pie: 'Switch to pies',
                                funnel: 'Switch to funnel',
                            },
                            type: ['pie', 'funnel']
                        },
                        saveAsImage: {
                            show: true,
                            title: 'Same as image',
                            lang: ['Save']
                        }
                    }
                },

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [
                    {
                        name: 'Increase (brutto)',
                        type: 'pie',
                        radius: ['15%', '73%'],
                        center: ['50%', '57%'],
                        roseType: 'radius',

                        // Funnel
                        width: '40%',
                        height: '78%',
                        x: '30%',
                        y: '17.5%',
                        max: 450,

                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true
                                },
                                labelLine: {
                                    show: true
                                }
                            }
                        },
                        data: [
                          {value: 260, name: 'Январь'},
                          {value: 440, name: 'Февраль'},
                          {value: 150, name: 'Март'},
                          {value: 250, name: 'Апрель'},
                          {value: 210, name: 'Май'},
                          {value: 350, name: 'Июнь'},
                        ]
                    }
                ]
            };

            // Apply options
            // ------------------------------

            rose_diagram_hidden.setOption(rose_diagram_hidden_options);


            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                    rose_diagram_hidden.resize();
                }, 200);
            }

            window.redrawRoseDiagram = () => {
              rose_diagram_hidden_options = {
                  series: [
                      {
                          data: [
                            {value: getRandomArbitrary(200, 300), name: 'Январь'},
                            {value: getRandomArbitrary(400, 500), name: 'Февраль'},
                            {value: getRandomArbitrary(250, 350), name: 'Март'},
                            {value: getRandomArbitrary(350, 450), name: 'Апрель'},
                            {value: getRandomArbitrary(150, 250), name: 'Май'},
                            {value: getRandomArbitrary(300, 400), name: 'Июнь'},
                          ]
                      }
                  ]
              };

              console.log(rose_diagram_hidden_options);

              rose_diagram_hidden.setOption(rose_diagram_hidden_options);
            }

            $('#pie-panel').toggle();
        }
    );
});
