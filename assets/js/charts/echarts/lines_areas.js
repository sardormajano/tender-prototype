/* ------------------------------------------------------------------------------
 *
 *  # Echarts - lines and areas
 *
 *  Lines and areas chart configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

window.getRandomArbitrary = (min, max) => Math.random() * (max - min) + min;

$(function() {


    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            var basic_lines = ec.init(document.getElementById('basic_lines'), limitless);

            basic_lines.on('click', (params) => {
              console.log(params);
            });


            // Charts setup
            // ------------------------------

            //
            // Basic lines options
            //

            basic_lines_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 40,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: [
                      'Всего жалоб', 'Принято', 'Отклонено'
                    ]
                },

                toolbox: {
                  show: true,
                  feature: {
                      magicType: {
                          show: true,
                          title: {
                              line: 'Switch to line chart',
                              bar: 'Switch to bar chart'
                          },
                          type: ['line', 'bar']
                      },
                      restore: {
                          show: true,
                          title: 'Restore'
                      },
                      saveAsImage: {
                          show: true,
                          title: 'Same as image',
                          lang: ['Save']
                      }
                  }
                },

                // Add custom colors
                color: ['#b6a2de', '#66BB6A', '#EF5350'],

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: ['Январь 2017', 'Февраль 2017', 'Март 2017', 'Апрель 2017', 'Май 2017', 'Июнь 2017']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        formatter: '{value}'
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Всего жалоб',
                        type: 'line',
                        data: [573, 476, 447, 519, 399, 261],
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Среднее количество'
                            }]
                        }
                    },
                    {
                        name: 'Принято',
                        type: 'line',
                        data: [460, 256, 419, 377, 309, 201],
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Среднее количество'
                            }]
                        }
                    },
                    {
                        name: 'Отклонено',
                        type: 'line',
                        data: [113, 222, 28, 53, 90, 60],
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Среднее количество'
                            }]
                        }
                    }
                ]
            };

            window.redrawBasicLines = () => {
              basic_lines_options = {

                  series: [
                      {
                          name: 'Всего жалоб',
                          type: 'line',
                          data: [
                            getRandomArbitrary(500, 600),
                            getRandomArbitrary(400, 500),
                            getRandomArbitrary(350, 450),
                            getRandomArbitrary(500, 600),
                            getRandomArbitrary(250, 350),
                            getRandomArbitrary(150, 250)
                          ],
                          markLine: {
                              data: [{
                                  type: 'average',
                                  name: 'Среднее количество'
                              }]
                          }
                      },
                      {
                          name: 'Принято',
                          type: 'line',
                          data: [
                            getRandomArbitrary(400, 500),
                            getRandomArbitrary(300, 400),
                            getRandomArbitrary(250, 350),
                            getRandomArbitrary(400, 500),
                            getRandomArbitrary(150, 250),
                            getRandomArbitrary(50, 150)
                          ],
                          markLine: {
                              data: [{
                                  type: 'average',
                                  name: 'Среднее количество'
                              }]
                          }
                      },
                      {
                          name: 'Отклонено',
                          type: 'line',
                          data: [
                            getRandomArbitrary(0, 100),
                            getRandomArbitrary(0, 100),
                            getRandomArbitrary(0, 100),
                            getRandomArbitrary(0, 100),
                            getRandomArbitrary(0, 100),
                            getRandomArbitrary(0, 100)
                          ],
                          markLine: {
                              data: [{
                                  type: 'average',
                                  name: 'Среднее количество'
                              }]
                          }
                      }
                  ]
              };

              basic_lines.setOption(basic_lines_options);
            }

            // Apply options
            // ------------------------------

            basic_lines.setOption(basic_lines_options);

            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function () {
                    basic_lines.resize();
                }, 200);
            }
        }
    );
});
