/* ------------------------------------------------------------------------------
*
*  # Basic datatables
*
*  Specific JS code additions for datatable_basic.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */


$(document).ready(function() {

    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [ 5 ]
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


    // Basic datatable
    const purchaseTable = $('.datatable-basic').dataTable({
  		"language": {
  		  "processing": "Подождите...",
  		  "search": "",
  		  "lengthMenu": "Показать _MENU_ записей",
  		  "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
  		  "infoEmpty": "Записи с 0 до 0 из 0 записей",
  		  "infoFiltered": "(отфильтровано из _MAX_ записей)",
  		  "infoPostFix": "",
  		  "loadingRecords": "Загрузка записей...",
  		  "zeroRecords": "Записи отсутствуют.",
  		  "emptyTable": "В таблице отсутствуют данные",
  		  "paginate": {
  			"first": "Первая",
  			"previous": "Предыдущая",
  			"next": "Следующая",
  			"last": "Последняя"
  		  },
  		  "aria": {
  			"sortAscending": ": активировать для сортировки столбца по возрастанию",
  			"sortDescending": ": активировать для сортировки столбца по убыванию"
  		  },
  		  typeToFilter: 'Поиск'
  		}
  	});

    //purchaseTable.fnFilter('В работе');

    $('#purchase-filter').on('change', (e) => {
      const {value} = e.currentTarget;

      switch(value) {
        case 'important':
          purchaseTable.fnFilter('В работе');
          break;
        case 'all':
          purchaseTable.fnFilter('');
          break;
        case 'completed':
          purchaseTable.fnFilter('Завершено');
          break;
        case 'completed-2':
          purchaseTable.fnFilter('Предоставлено');
          break;
        case 'blocked':
          purchaseTable.fnFilter('886456');
          break;
        case 'in-progress':
          purchaseTable.fnFilter('В работе');
          break;
        case 'pending':
          purchaseTable.fnFilter('Ожидается');
          break;
        case 'blocked-2':
          purchaseTable.fnFilter('986775');
          break;
      }
    });

    // Alternative pagination
    $('.datatable-pagination').DataTable({
        pagingType: "simple",
        language: {
            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
        }
    });


    // Datatable with saving state
    $('.datatable-save-state').DataTable({
        stateSave: true
    });


    // Scrollable datatable
    $('.datatable-scroll-y').DataTable({
        autoWidth: true,
        scrollY: 300
    });

    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

});
