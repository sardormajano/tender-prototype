/* ------------------------------------------------------------------------------
*
*  # Select2 selects
*
*  Specific JS code additions for form_select2.html page
*
*  Version: 1.1
*  Latest update: Nov 20, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {
  $('#organizations').select2({
      placeholder: 'По умолчанию выбраны все компании',
      minimumResultsForSearch: Infinity
  });

  $('#organizations').on('select2:select', () => {
    window.redrawBasicLines();
    window.redrawRoseDiagram();
  });
  
	$('#executors-list').select2({
        placeholder: 'Выберите исполнителя',
        minimumResultsForSearch: Infinity
    });

	$('#violations-list').select2({
        placeholder: 'Выберите пункты нарушения',
        minimumResultsForSearch: Infinity
    });

	$('#tenders-list').select2({
        placeholder: 'Выберите закупку или введите номер закупки',
        minimumResultsForSearch: 4
    });

	$('#customers-list').select2({
        placeholder: 'Выберите заказчика',
        minimumResultsForSearch: 4
    });

	$('#clients-list').select2({
        placeholder: 'Выберите заказчика',
        minimumResultsForSearch: 4
    });

	$('#suppliers-list').select2({
        placeholder: 'Выберите поставщика',
        minimumResultsForSearch: 4
    });

    $('#organizations').on('select2:select', () => {
      window.redrawBasicLines();
      window.redrawRoseDiagram();
    });

    $('#diagram-type').select2({
        placeholder: 'Выберите тип диаграммы',
        minimumResultsForSearch: Infinity
    });

    $('#diagram-type').on('select2:select', (e) => {
      $('#pie-panel, #line-panel').toggle();
    });
});
